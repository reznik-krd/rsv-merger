package main;

import org.w3c.dom.Document;

/**
 * Data for RSV of two files, singleton
 */
public final class DataRSV {
    private static DataRSV instance;
    private Document one;
    private Document two;
    private Document result;

    private DataRSV() {
    }

    public static DataRSV getInstance() {
        if (instance == null) {
            instance = new DataRSV();
        }
        return instance;
    }

    public Document getOne() {
        return one;
    }

    public void setOne(Document one) {
        this.one = one;
    }

    public Document getTwo() {
        return two;
    }

    public void setTwo(Document two) {
        this.two = two;
    }

    public Document getResult() {
        return result;
    }

    public void setResult(Document result) {
        this.result = result;
    }
}
