package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.InputStream;
import java.util.Objects;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        InputStream icon = this.getClass().getClassLoader().getResourceAsStream("myicon.png");
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));

        primaryStage.setTitle("РСВ помощник от Резник Н.А.");
        primaryStage.setScene(new Scene(root, 500, 300));
        if (Objects.nonNull(icon))
            primaryStage.getIcons().add(new Image(icon));

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
