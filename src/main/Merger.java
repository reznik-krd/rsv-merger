package main;

import org.w3c.dom.*;
import main.exceptions.FailedCalculateAmountsException;
import main.exceptions.FailedCopyPeople;

import java.util.*;

public class Merger {

    private static final Map<String, String> rules = new HashMap<>();
    private static final String SEARCH_PEOPLE = "ПерсСвСтрахЛиц";
    private static final String SEARCH_PLACE_INSERT_PEOPLE = "РасчетСВ";
    private static final String FIELD_MERGER = "ОбязПлатСВ";
    private static final String FORMAT_0 = "%.0f";
    private static final String FORMAT_2 = "%.2f";

    static {
        rules.put("СумСВУплПер", FORMAT_2);
        rules.put("СумСВУпл1М", FORMAT_2);
        rules.put("СумСВУпл2М", FORMAT_2);
        rules.put("СумСВУпл3М", FORMAT_2);
        rules.put("ПревРасхСВ1М", FORMAT_2);
        rules.put("КолВсегоПер", FORMAT_0);
        rules.put("Кол1Посл3М", FORMAT_0);
        rules.put("Кол2Посл3М", FORMAT_0);
        rules.put("Кол3Посл3М", FORMAT_0);
        rules.put("СумВсегоПер", FORMAT_2);
        rules.put("Сум1Посл3М", FORMAT_2);
        rules.put("Сум2Посл3М", FORMAT_2);
        rules.put("Сум3Посл3М", FORMAT_2);
        rules.put("Сумма", FORMAT_2);
        rules.put("ЧислСлуч", FORMAT_0);
        rules.put("КолВыпл", FORMAT_0);
        rules.put("РасхВсего", FORMAT_2);
        rules.put("РасхФинФБ", FORMAT_2);
    }

    private Merger() {
    }

    public static void insertPeople() throws FailedCopyPeople {
        try {
            DataRSV dataRSV = DataRSV.getInstance();
            List<Node> people = new ArrayList<>();
            findChildren(dataRSV.getTwo().getDocumentElement(), people, SEARCH_PEOPLE);
            Node pointInsert = findChild(dataRSV.getResult().getDocumentElement(), SEARCH_PLACE_INSERT_PEOPLE);
            if (Objects.nonNull(pointInsert)) {
                people.forEach(element -> {
                    Node importedNode = dataRSV.getResult().importNode(element, true);
                    pointInsert.appendChild(importedNode);
                });
            }
        } catch (Exception ex) {
            throw new FailedCopyPeople();
        }
    }

    public static void mergeResource() throws FailedCalculateAmountsException {
        try {
            DataRSV dataRSV = DataRSV.getInstance();
            Node result = findChild(dataRSV.getResult().getDocumentElement(), FIELD_MERGER);
            Node two = findChild(dataRSV.getTwo().getDocumentElement(), FIELD_MERGER);
            if (Objects.isNull(result) || Objects.isNull(two)) {
                return;
            }
            merge(result, two);
        }
        catch (Exception ex) {
            throw new FailedCalculateAmountsException();
        }
    }

    private static void findChildren(Node parent, List<Node> elements, String name) {
        NodeList children = parent.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (name.equals(child.getNodeName())) {
                elements.add(child);
            }
            findChildren(child, elements, name);
        }
    }

    private static Node findChild(Node parent, String name) {
        NodeList children = parent.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child instanceof Element && name.equals(child.getNodeName())) {
                return child;
            } else {
                Node childInner = findChild(child, name);
                if (Objects.nonNull(childInner))
                    return childInner;
            }
        }
        return null;
    }

    //обязуемся, что эти блоки точно идентичны
    private static void merge(Node result, Node two) {
        NodeList resultChildren = result.getChildNodes();
        NodeList twoChildren = two.getChildNodes();

        for (int i = 0; i < resultChildren.getLength(); i++) {
            Node child1 = resultChildren.item(i);
            Node child2 = twoChildren.item(i);
            NamedNodeMap attributes1 = child1.getAttributes();
            NamedNodeMap attributes2 = child2.getAttributes();
            if (Objects.nonNull(attributes1) && Objects.nonNull(attributes2)) {
                rules.keySet().forEach(rule -> {
                    if (Objects.nonNull(attributes1.getNamedItem(rule)) && Objects.nonNull(attributes2.getNamedItem(rule))) {
                        String oneSum = attributes1.getNamedItem(rule).getNodeValue();
                        String twoSum = attributes2.getNamedItem(rule).getNodeValue();
                        attributes1.getNamedItem(rule).setNodeValue(String.format(Locale.ROOT, rules.get(rule), (Double.parseDouble(oneSum) + Double.parseDouble(twoSum))));
                    }
                });
            }
            merge(child1, child2);
        }
    }
}
