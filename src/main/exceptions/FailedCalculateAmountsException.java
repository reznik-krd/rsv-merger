package main.exceptions;

public class FailedCalculateAmountsException extends Exception {

    private static final String REASON = "Не удалось посчитать суммы";

    public FailedCalculateAmountsException() {
        super(REASON);
    }
}
