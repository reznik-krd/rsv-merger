package main.exceptions;

public class FailedCopyPeople extends Exception {

    private static final String REASON = "Не удалось скопировать людей";

    public FailedCopyPeople() {
        super(REASON);
    }
}
