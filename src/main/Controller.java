package main;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import main.exceptions.FailedCalculateAmountsException;
import main.exceptions.FailedCopyPeople;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

public class Controller {

    @FXML
    private Button btnMerge;
    @FXML
    private TextArea labelErrorMessage;
    @FXML
    private Label fileMainName;
    @FXML
    private Label fileJoinName;
    @FXML
    private Label fileCommonName;

    @FXML
    private void selectMainFile() {
        try {
            File file = selectFile();
            if (Objects.nonNull(file)) {
                DataRSV.getInstance().setOne(fileToDocument(file));
                fileMainName.setText(file.getName());
            }
        } catch (ParserConfigurationException | IOException | SAXException ex) {
            labelErrorMessage.setText("Не удалось считать файл., ошибка: " + ex.getMessage());
        }
        provideMerge();
    }

    @FXML
    private void selectJoinFile() {
        try {
            File file = selectFile();
            if (Objects.nonNull(file)) {
                DataRSV.getInstance().setTwo(fileToDocument(file));
                fileJoinName.setText(file.getName());
            }
        } catch (ParserConfigurationException | IOException | SAXException ex) {
            labelErrorMessage.setText("Не удалось считать файл, ошибка: " + ex.getMessage());
        }
        provideMerge();
    }

    @FXML
    private void getCommonFile() {
        try {
            copyOneToResult();
            Merger.insertPeople();
            Merger.mergeResource();
            saveDocument();
            provideMerge();
        } catch (ParserConfigurationException | TransformerException | IOException | FailedCopyPeople | FailedCalculateAmountsException ex) {
            labelErrorMessage.setText("Ошибка сохранения результата: " + ex.getMessage());
        }
    }

    private void copyOneToResult() throws ParserConfigurationException {
        Document one = DataRSV.getInstance().getOne();
        Node oneRoot = one.getDocumentElement();

        DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document result = db.newDocument();
        Node resultRoot = result.importNode(oneRoot, true);
        result.appendChild(resultRoot);

        DataRSV.getInstance().setResult(result);
    }

    private File selectFile() {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Выбор файла для объединения");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml"));
        return fileChooser.showOpenDialog(btnMerge.getScene().getWindow());
    }

    private Document fileToDocument(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        return documentBuilder.parse(file);
    }

    private void provideMerge() {
        btnMerge.setDisable(Objects.isNull(DataRSV.getInstance().getOne())
                || Objects.isNull(DataRSV.getInstance().getTwo()));
    }

    private void saveDocument() throws TransformerException, IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Сохранить итоговый файл");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showSaveDialog(btnMerge.getScene().getWindow());
        if (Objects.nonNull(file)) {
            Transformer tr = TransformerFactory.newInstance().newTransformer();
            DOMSource source = new DOMSource(DataRSV.getInstance().getResult());
            FileOutputStream fos = new FileOutputStream(file.getPath());
            StreamResult result = new StreamResult(fos);
            tr.transform(source, result);
            fos.close();

            fileCommonName.setText(file.getName());
        }
    }
}
